@echo off
setlocal

cd /d %~dp0

set win_version=1909-amd64
set vs_version_major=16
set vs_version_minor=6

if not defined CI_REGISTRY_IMAGE (
    set CI_REGISTRY_IMAGE=docker-windows-build
)

docker build --pull --tag "%CI_REGISTRY_IMAGE%:win%win_version%-vs2019-%vs_version_major%.%vs_version_minor%" . --build-arg "win_version=%win_version%" --build-arg "vs_version_major=%vs_version_major%"
