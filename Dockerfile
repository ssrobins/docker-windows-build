ARG win_version
FROM mcr.microsoft.com/windows/servercore:$win_version

# Visual Studio Build Tools
ARG vs_version_major
ARG vs_installer=vs_buildtools.exe
ADD https://aka.ms/vs/$vs_version_major/release/$vs_installer $vs_installer
RUN %vs_installer% --quiet --wait \
--add Microsoft.VisualStudio.Component.VC.Tools.x86.x64 \
--add Microsoft.VisualStudio.Component.Windows10SDK.19041

# CMake
ARG cmake_version=3.18.3
ARG cmake_installer=cmake-$cmake_version-win64-x64.msi
ADD https://github.com/Kitware/CMake/releases/download/v$cmake_version/$cmake_installer $cmake_installer
RUN msiexec /package %cmake_installer% ADD_CMAKE_TO_PATH=System /passive
RUN cmake --version

# Python
ARG python_version=3.8.6
ARG python_installer=python-$python_version.exe
ADD https://www.python.org/ftp/python/$python_version/$python_installer $python_installer
RUN %python_installer% /quiet PrependPath=1
RUN python --version

# Conan
ARG conan_version=1.29.2
RUN pip3 install conan==%conan_version%
RUN conan --version
